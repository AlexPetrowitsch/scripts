#Installation:  
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

mv vimrc ~/.vimrc

vim  
:PluginInstall  

or  
vim +PluginInstall +qall

To unlock youcompleteme (python-dev has to be installed):
cd ~/.vim/bundle/YouCompleteMe  
./install.py --clang-completer  

#shortcuts  
- Explorer toggle with strg+n
- alt+arrows move between windows when split or explorer used
- F4 move between header and source
