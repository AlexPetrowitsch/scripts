# Configure bash to use git much easyer

## Add git branch in prompt
The following steps are done in ~/.bashrc file.

- Enable **force_color_prompt=yes**

- Insert the following function in top of the prompt definition.
```
parse_git_branch() {
 git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
```
- Adapt prompt to insert git branch. Therefore include call of function **parse_git_branch** with and without color.
```
if [ "$color_prompt" = yes ]; then
 PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\] $(parse_git_branch)\[\033[00m\]\$ '
else
 PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w$(parse_git_branch)\$ '
fi
```

## Add git alias
- At the end of the ~/.bashrc file insert the following lines:
```
if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi
```

- Copy the .bash_alias file to the home directory.
**cp ./.bash_aliases ~/.bash_aliases**

You can look at the .bash_aliases file which aliases exists. Also you can extend the file for more aliases.

## Activate settings
Restart the terminal ;-)
