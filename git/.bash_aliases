# --------------
# Git functions
# --------------

# Git log find by commit message
function glf() { 
  git log --all --grep="$1"
}

# Function to execute a git command followed by "git status"
function git-command-with-status {
  git $*
  git status
}

# Function to set an alias for a git command and get status after execution
function git-alias {
  aliasname=$1
  shift
  alias $aliasname="git-command-with-status $*"
}

# ------------
# Git aliases
# ------------

alias gco='git checkout'
alias gcob='git checkout -b'
alias gcom='git checkout master'
alias gst='git status'
alias gcm='git commit -m'
alias pull='git pull'
alias pullreb='git pull --rebase'
alias grm='git rm'

# detailed one-line view of git log
alias gld='git log --pretty=format:"%h %ad %s" --date=short --all'
# detailed decoration graph of git log
alias glg='git log --graph --oneline --decorate --all'

# with status after command
git-alias add add
git-alias push push
